/**
 * ---------- ПРИМЕРЫ ES6 --------------- *
 *             ЗАМЫКАНИЯ                  *
 * -------------------------------------- *
 */

function runCounter() {
    let count = 0;
    setInterval(
        () => {
            console.log(count++);
        },
        1000
    );
}
console.log("start script");
runCounter();
console.log("end script");
//Вывод: start script, end script, 1,2,3,4 …

// такая функция будет возвращать другую функцию
// при этом переданное значение n “замыкается”
function makeIncrementer(n) {
    return (num) => num += n;
}
// Возвращаемые функции помнят контекст где были созданы
// замкнем n
let incByOne = makeIncrementer(1);
let incByFour = makeIncrementer(4);

console.log(incByOne(10)); // 11
console.log(incByFour(10)); // 14
