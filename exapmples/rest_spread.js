/**
 * ---------- ПРИМЕРЫ ES6 --------------- *
 *           REST & SPREAD                *
 * -------------------------------------- *
 */

/**
 * -------------- REST ------------------ *
 *        ОСТАТОЧНЫЙ ПАРАМЕТР             *
 * -------------------------------------- *
 */
function mySumm(a, ...arr) {
    let result = a;
    for (let item of arr) {
        result += item;
    }
    return result;
}
// 15
console.log(mySumm(0, 1, 2, 3, 4, 5));
// 6
console.log(mySumm(0, 1, 2, 3));

/**
 * ------------- SPREAD ----------------- *
 *        ОПЕРАТОР РАСШИРЕНИЯ             *
 * ------- в вызовах функции ------------ *
 */
// Например функция Math.max() принимает список параметров
// чтобы передать их в ES5
Math.max.apply(Math, [2, 100, 1, 6, 43]) // 100

// В ES6 используем spread:
Math.max(...[2, 100, 1, 6, 43]) // 100

/**
 * ------------- SPREAD ----------------- *
 *        ОПЕРАТОР РАСШИРЕНИЯ             *
 * ----- расширения в массивах ---------- *
 */
let newArr = [3, 4, 5];
let arr = [1, 2, ...newArr, 6];
// [ 1, 2, 3, 4, 5, 6 ]
console.log(arr);
