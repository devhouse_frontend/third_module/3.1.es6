/**
 * ---------- ПРИМЕРЫ ES6 --------------- *
 *         шаблонные строки               *
 * -------------------------------------- *
 */
// в ES5 необходимо было писать так
// var firstName = 'Vasya ';
// var secondName = 'Petrov';
// // Your name - Vasya Petrov
// console.log("Your name - " + firstName + secondName);

// в ES6 можно делать так
let firstName = 'Vasya';
let secondName = 'Petrov';
// Your name - Vasya  Petrov
console.log(`Your name - ${firstName} ${secondName}`);